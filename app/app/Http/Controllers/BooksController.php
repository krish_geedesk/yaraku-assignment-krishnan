<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Books;
use App\Models\Authors;
use Exception;
use Response;
use DB;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller
{

    /**
     * @param string book_title Title of the book
     * @param string book_author Author of the book
     *
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @return Boolean success or failure message
     */
    public function addNewBook(Request $request) 
    {

        // prepare Validator to validate the input
        $validator = validator($request->all(), [
            "book_title"    => "required",
            "book_author"    => "required",
        ], [
            "book_title.required"   => "Book title cannot be empty",
            "book_author.required"  => "Book author cannot be empty",
        ]);

        // if validation fails
        if($validator->fails()) {
            return response()->json(["success" => false, "errors" => $validator->errors()->first()], 200);
        }

        $createdBookId = 0;
        $bookStatus = '';
        
        $rawBookTitle = $request->post('book_title');
        $bookTitle = preg_replace("/[^a-zA-Z ]/", "", $rawBookTitle);

        $rawBookAuthor = $request->post('book_author');
        $bookAuthor = preg_replace("/[^a-zA-Z ]/", "", $rawBookAuthor);

        if (!empty($bookTitle) && !empty($bookAuthor)) 
        {
            $books = new Books;
            $authors = new Authors;

            //Checking if the book already exists
            $bookExists = $books->where([
                   'title' => $bookTitle
            ])->first();

            //Checking if the author already exists
            $author = $authors->where([
                   'name' => $bookAuthor
            ])->first();

            if ($author === null) {
                $authorId = Authors::insertGetId([
                    'name' => $bookAuthor
                ]);
            }
            else {
                $authorId = $author->id;
            }


            if ($bookExists === null) {
                $createdBookId = Books::insertGetId([
                    'author_id' => $authorId,
                    'title' => $bookTitle
                ]);
            }
            else {
                $bookStatus = 'duplicate';
            }

            if ($createdBookId > 0 && $bookStatus === '') {
                return response()->json(['status' => 'success', 'message'=> 'Book created successfully'], 200);
            }
            elseif ($createdBookId === 0 && $bookStatus === 'duplicate') {
                return response()->json(['status' => 'failed', 'message'=> 'A book with same title already exists'], 200);
            }
            else {
                return response()->json(['status' => 'failed', 'message'=> 'Your book could not be created due to an error, please try again or reach out to us at support@yaraku.com'], 200);
            }
        }
    }


    /**
     * @param NIL
     *
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @return All books and authors
     */
    public function allBooks()
    {
        $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->orderBy('id')
                ->get();

        if ($books !== null && is_object($books) && is_countable($books)) 
        {
            return response()->json(['status' => 'success', 'books' => $books], 200);
        }
        else
        {
            return response()->json(['status' => 'failed', 'message'=> 'No books found'], 200);
        }
    }



    /**
     * @param integer book_id ID of the book
     *
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @return Boolean success or failure message
     */
    public function deleteBook(Request $request)
    {
        // prepare Validator to validate the input
        $validator = Validator::make($request->all(), [
            "book_id" => "required|exists:books,id"
        ], [
            "book_id.required" => "Book ID cannot be empty",
            "book_id.exists"   => "Invalid book id"
        ]);


        // if validation fails
        if($validator->fails()) {
            return response()->json(["success" => false, "errors" => $validator->errors()->first()], 200);
        }
        
        $bookId = $request->post('book_id');

        if (!empty($bookId) && is_numeric($bookId)) 
        {
            $book = Books::find($bookId);
            $bookDeleted = $book->delete($bookId);

            if ($bookDeleted) {
                  return response()->json(['status' => 'success', 'message'=> 'Book deleted successfully'], 200);
            }
            else 
            {
                return response()->json(['status' => 'failed', 'message'=> 'Your book could not be created due to an error, please try again or reach out to us at support@yaraku.com'], 200);
            }
        }
    }



    /**
     * @param integer book_id ID of the book
     * @param string book_author Author of the book
     *
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @return Boolean success or failure message
     */
    public function updateAuthorName(Request $request)
    {
        // prepare Validator to validate the input
        $validator = Validator::make($request->all(), [
            "book_id" => "required|exists:books,id", 
            "author_name"    => "required",
        ], [
            "book_id.required" => "Book ID cannot be empty",
            "book_id.exists"   => "Invalid book id", 
            "author_name.required"  => "Book author cannot be empty",
        ]);


        // if validation fails
        if($validator->fails()) {
            return response()->json(["success" => false, "errors" => $validator->errors()->first()], 200);
        }

        $authorId = 0;
        $bookId = $request->post('book_id');
        $rawNewAuthor = $request->post('author_name');
        $newAuthor = preg_replace("/[^a-zA-Z ]/", "", $rawNewAuthor);

        $authors = new Authors;
        $author = $authors->where([
                   'name' => $newAuthor
            ])->first();

        if ($author === null) {
            $authorId = Authors::insertGetId([
                'name' => $newAuthor
            ]);
        }
        else {
            $authorId = $author->id;
        }


        if ($authorId > 0) {

            $book = Books::find($bookId);

            $book->author_id = $authorId;

            $bookAuthorUpdated = $book->save();

            if ($bookAuthorUpdated) {
                return response()->json(['status' => 'success', 'message'=> 'Book author updated successfully'], 200);
            }
            else {
                return response()->json(['status' => 'failed', 'message'=> 'Your book could not be created due to an error, please try again or reach out to us at support@yaraku.com'], 200);
            }
        }
        else {
            return response()->json(['status' => 'failed', 'message'=> 'Invalid request`'], 200);
        }
    }



    /**
     * Search a book based on the query
     * 
     * @param $query string the query based on which the books and authors have to searched
     * 
     * @author Krishnan skrishnan784@gmail.com
     * 
     * @return JSON
     *
     */
    public function searchBooksAndAuthors(Request $request)
    {

        $rawSearchQuery = $request->get('query');
        $searchQuery = preg_replace("/[^a-zA-Z ]/", "", $rawSearchQuery);

        if (!empty($searchQuery) && strlen($searchQuery) > 0) {
            $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->where('books.title', 'like', "%{$searchQuery}%")
                ->orWhere('authors.name', 'like', "%{$searchQuery}%")
                ->get();

            if ($books !== null && is_object($books) && is_countable($books)) 
            {
                return response()->json(['status' => 'success', 'books' => $books], 200);
            }
            else
            {
                return response()->json(['status' => 'failed', 'message'=> 'No books found'], 200);
            }
        }
        else {
            return response()->json(['status' => 'failed', 'message'=> 'The search query was empty'], 200);
        }
    }



    /**
     * Search a book based on the query
     * 
     * @param $sort_by string the sort parameter based on which the the books or authors are sorted. 
     * 
     * @author Krishnan skrishnan784@gmail.com
     * 
     * @return JSON
     *
     */
    public function sortBooks(Request $request)
    {
        $rawSortBy = $request->get('sort_by');
        $sortBy = preg_replace("/[^a-zA-Z ]/", "", $rawSortBy);

        $books = '';

        if (!empty($sortBy) && strlen($sortBy) > 0 && ($sortBy !== 'titles' || $sortBy !== 'authors')) 
        {
            if ($sortBy === 'titles') {
                $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->orderBy('books.title')
                ->get();
            }
            elseif ($sortBy === 'authors') {
                $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->orderBy('authors.name')
                ->get();
            }

            if ($books !== null && is_object($books) && is_countable($books)) 
            {
                return response()->json(['status' => 'success', 'books' => $books], 200);
            }
            else
            {
                return response()->json(['status' => 'failed', 'message'=> 'No books found'], 200);
            }
        }
        else {
            return response()->json(['status' => 'failed', 'message'=> 'No books found or wrong sort by value provided'], 200);
        }
    }

}
