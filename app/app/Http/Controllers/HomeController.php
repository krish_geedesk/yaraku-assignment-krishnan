<?php

namespace App\Http\Controllers;
use App\Models\Books;
use App\Models\Authors;
use Storage;
use DOMDocument;
use Response;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    
    /**
     * Home view
     * 
     * @param nil
     *
     * @author Krishnan skrishnan784@gmail.com
     *
     */
    public function index() 
    {
        return view('home.home_view');
    }



    /**
     * Download list of books in csv format
     * 
     * @param $request - parameter to get the download type
     *
     * @author Krishnan skrishnan784@gmail.com
     * 
     * @return Download list of books in csv format
     *
     */
    public function csv_download(Request $request)
    {
        $action = $request->get('action');
        
        $cur_date = Date('Y_m_d_H:i:s');

        $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->orderBy('id')
                ->get();

        $books = $books->toArray();

        if (is_array($books) && count($books) > 0) {
            if ($action == 'titles')
            {
                $file_name = 'books_titles_'.$cur_date.'.csv';
                $headers = array('Title');
            }
            elseif ($action == 'authors') 
            {
                $file_name = 'books_authors_'.$cur_date.'.csv';
                $headers = array('Author');
            }
            else
            {
                $file_name = 'books_'.$cur_date.'.csv';
                $headers = array('Title', 'Author');
            }

            $file_loc = $file_name;

            // file creation
            $file = fopen($file_loc,"w");

            //Adding the headers to the csv file
            fputcsv($file, $headers);

            $line = array();

            foreach ($books as $row)
            {
                if ($action == 'all') 
                {
                    $line = array($row['title'], $row['name']);
                }
                elseif ($action == 'titles')
                {
                    $line = array($row['title']);
                }
                elseif ($action == 'authors') 
                {
                    $line = array($row['name']);
                }

                fputcsv($file, $line);
            }

            fclose($file);

            // download
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=".$file_name);
            header("Content-Type: application/csv; "); 

            readfile($file_loc);
            unlink($file_loc);
        }
    }



    /**
     * Download list of books in xml format
     * 
     * @param $request - parameter to get the download type
     *
     * @author Krishnan skrishnan784@gmail.com
     * 
     * @return Download list of books in xml format
     *
     */
    public function xml_download(Request $request)
    {
        $action = $request->get('action');

        $books = Books::join('authors', 'books.author_id', '=', 'authors.id')
                ->select('books.id', 'books.title', 'authors.name')
                ->orderBy('id')
                ->get();

        $books = $books->toArray();

        if (is_array($books) && count($books) > 0) {
            $cur_date = Date('Y_m_d');

            $xml = new DOMDocument("1.0"); 
      
            $xml->formatOutput=true;  
            $xml_file=$xml->createElement("books"); 
            $xml->appendChild($xml_file); 
            
            foreach ($books as $row) 
            {
                $element = $xml->createElement("book"); 
                $xml_file->appendChild($element); 

                $title = $xml->createElement("title", $row['title']); 

                $author = $xml->createElement("author", $row['name']); 

                if ($action == 'titles') 
                {
                    $file_name = 'books_titles_'.$cur_date.'.xml';

                    $element->appendChild($title); 
                }
                elseif ($action == 'authors') 
                {
                    $file_name = 'books_authors_'.$cur_date.'.xml';

                    $element->appendChild($author);
                }
                else 
                {
                    $file_name = 'books_'.$cur_date.'.xml';

                    $element->appendChild($title); 
                    $element->appendChild($author);
                }
            }

            $file_loc = $file_name;

            $xml->save($file_loc);

            //download
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=".$file_name);
            header("Content-Type: application/xml; "); 

            readfile($file_loc);
            unlink($file_loc);
        }
    }

}
