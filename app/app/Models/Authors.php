<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name'];

    /**
    * Get the books record associated with the author.
    */
    public function books()
    {
        return $this->hasMany(Books::class, 'author_id', 'id', 'title');
    }

}
