<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::prefix('books')->group(function () {
    Route::get('download/csv', 'App\Http\Controllers\HomeController@csv_download');
	Route::get('download/xml', 'App\Http\Controllers\HomeController@xml_download');
});