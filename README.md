# README #
## Installation
Clone the repository with the following instructions
- `git clone git@bitbucket.org:krish_geedesk/yaraku-assignment-krishnan.git`
- Navigate to app directory and run the following commands
-- `composer install`
-- `npm install`
- Create Database (DB name: booksapp)
-  Update DB configs (name, server, password) in .env 
-  Run `php artisan migrate` to provision the tables

## DB Architecture
The database schema and architecture can be studied by uploading the `bookapp-v{lasted version number}.xml` to sql designer. 

You can setup your own sql designer instance locally or use any of the online ones.

## Automated Testing 
The API test cases are available in db_model/postman_collections
- The postman collection tests can be run via `newman run {postman file name}`

## About Book App

Book App - Anonymously store and share your books with the world

API url:
https://yaraku.theservermechanic.com/api

Public URL:
https://yaraku.theservermechanic.com
